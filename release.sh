#!/bin/sh
name="yugen-rs"
arch="x86_64-pc-windows-gnu"
# arch="x86_64-unknown-linux-gnu"
RUSTFLAGS="-Zlocation-detail=none" cargo +nightly build -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort \
    --release --target $arch
upx --best --lzma target/$arch/release/$name.exe
cp target/$arch/release/$name.exe yugen-rs.exe
