use crate::{AnilistError, IOError, YugenError};

#[derive(Debug)]
pub enum Error {
    Yugen(YugenError),
    Anilist(AnilistError),
    IO(IOError),
}

use std::fmt;
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use AnilistError::*;
        use IOError::*;
        use YugenError::*;
        match self {
            Error::Yugen(e) => match e {
                DownloadSearchResult(e) => write!(f, "failed to search page: {:?}", e),
                DownloadFromId(e) => write!(f, "failed to download from id: {:?}", e),
                DownloadEpPage(e) => write!(f, "failed to download the episode page: {:?}", e),
                IdNotFound => write!(f, "did not find the id on the episode page"),
                EpLinkResponse(e) => write!(f, "failed to get episode linke response: {:?}", e),
                VideoLinkNotFound => write!(f, "video link not found"),
                EpTitleNotFound => write!(f, "episode title not found"),
                EmptySearchResult => write!(f, "no search results for query"),
            },
            Error::Anilist(e) => match e {
                AnilistFailedToFetchData(e) => {
                    write!(f, "failed to fetch data from anilist: {:?}", e)
                }
                NoAnimeFound => write!(f, ""),
                EmptyWatchingList => write!(f, "found no anime in currently watching list"),
                FailedToConvertId => write!(f, "failed to convert anilist id to yugen id"),
                BalMackupDataNotFound(e) => write!(f, "failed to get bal mackup data: {:?}", e),
                FailedToFetchProgress => {
                    write!(f, "failed to get episode progress for anime: {:?}", e)
                }
                FailedToUpdateProgress(e) => {
                    write!(f, "failed to update episode progress for anime: {:?}", e)
                }
                FetchUserIdData(e) => write!(f, "failed to fetch user id data: {:?}", e),
                GetUserIdData => write!(f, "failed to get user id from response"),
                FetchSearchResult(e) => write!(f, "failed to fetch anilist search result: {:?}", e),
                GetSearchResult => write!(f, "failed to get anilist search result"),
                ScoreOnCompletion(e) => write!(f, "failed to set score: {:?}", e),
            },
            Error::IO(e) => match e {
                MpvSpawn(e) => write!(f, "failed to spawn mpv: {:?}", e),
                MpvOutput(e) => write!(f, "failed to get mpv output: {:?}", e),
                MpvInvalidUtf8(e) => write!(f, "mpv output was invalid utf8: {:?}", e),
                FlushStdout(e) => write!(f, "failed to flush standard output: {:?}", e),
                ReadLine(e) => write!(f, "failed to read input: {:?}", e),
                ParseInput(e) => write!(f, "failed to parse input as number: {:?}", e),
                ParseMpvOutput => write!(f, "failed to get watch percentage from mpv output"),
                ParseProgress(e) => write!(f, "failed to parse progress from anilist: {:?}", e),
                ReadUserIdFile(e) => write!(f, "failed to read the user id file: {:?}", e),
                ReadTokenFile(e) => write!(f, "failed to read the token file: {:?}", e),
                AnimeChoiceOutOfBounds => write!(f, "anime choice was out of bounds"),
                DataDirNotFound => write!(f, "data dir to store token and id not found"),
                CreateDataDir(e) => write!(f, "failed to create new data dir: {:?}", e),
                CreateToken(e) => write!(f, "failed to create new token: {:?}", e),
                CreateUserId(e) => write!(f, "failed to create new user id: {:?}", e),
            },
        }
    }
}
