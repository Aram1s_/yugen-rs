use crate::anilist::*;
use crate::yugen::*;
use crate::Error;
use html_escape::decode_html_entities;
use regex::Regex;
use reqwest::Client;

#[derive(Debug)]
pub enum IOError {
    MpvSpawn(std::io::Error),
    MpvOutput(std::io::Error),
    MpvInvalidUtf8(std::string::FromUtf8Error),
    FlushStdout(std::io::Error),
    ReadLine(std::io::Error),
    ParseInput(std::num::ParseIntError),
    ParseMpvOutput,
    ParseProgress(std::num::ParseIntError),
    ReadTokenFile(std::io::Error),
    ReadUserIdFile(std::io::Error),
    AnimeChoiceOutOfBounds,
    DataDirNotFound,
    CreateDataDir(std::io::Error),
    CreateToken(std::io::Error),
    CreateUserId(std::io::Error),
}

pub fn get_input(msg: &str) -> Result<String, Error> {
    use std::{io, io::Write};

    print!("{msg}");

    io::stdout()
        .flush()
        .map_err(|e| Error::IO(IOError::FlushStdout(e)))?;

    let mut input = String::new();

    io::stdin()
        .read_line(&mut input)
        .map_err(|e| Error::IO(IOError::ReadLine(e)))?;

    Ok(input.trim().to_owned())
}

pub async fn run(client: &Client, token: &str, user_id: &str) -> Result<(), Error> {
    let data = get_anilist_data(client, user_id).await?;

    let (yugen_id, yugen_slug, watch_result) = get_ids(client, &data).await?;

    let mut progress = get_progress(&data, &watch_result.media_id.to_string())?
        .parse()
        .map_err(|e| Error::IO(IOError::ParseProgress(e)))?;

    loop {
        let status = if watch_result.episodes == progress + 1 {
            "COMPLETED"
        } else {
            &watch_result.status
        };

        let (video_url, ep_title) =
            get_video_url_and_title(client, &yugen_id, &yugen_slug, progress).await?;

        let mpv_output = play_video(&video_url, &ep_title)?;
        let watch_percentage =
            parse_mpv_output(&mpv_output).ok_or(Error::IO(IOError::ParseMpvOutput))?;

        if watch_result.episodes == progress + 1 {
            let score = get_input("Enter new score: ").unwrap().parse().unwrap();
            score_on_completion(client, score, token, watch_result.media_id)
                .await
                .unwrap();
        }

        if watch_percentage > 85 {
            update_progress(
                client,
                token,
                status,
                progress + 1,
                &watch_result.media_id.to_string(),
            )
            .await?;
        }

        if get_input("Continue: ")? != "y" {
            break;
        }

        progress += 1;
    }
    Ok(())
}

async fn get_video_url_and_title(
    client: &Client,
    yugen_id: &str,
    yugen_slug: &str,
    progress: u32,
) -> Result<(String, String), Error> {
    let ep_page = download_from_id(client, &yugen_id, &yugen_slug, progress).await?;
    let ep_title = get_ep_title(&ep_page, progress + 1)?;
    let ep_title = decode_html_entities(&ep_title);
    let yugen_id = get_id(&ep_page)?;
    let link_response = get_ep_link_response(client, &yugen_id).await?;
    let video_url = parse_link_response(&link_response)?;
    Ok((video_url, ep_title.to_string()))
}

pub async fn watch_new(
    client: &Client,
    token: &str,
    anilist_result: &AnilistResult,
) -> Result<(), Error> {
    let bal_data = bal_mackup_data(client, &anilist_result.id.to_string()).await?;
    let (yugen_id, yugen_slug) = anilist_to_yugen(&bal_data).await?;

    let mut progress = 0;

    let episodes = get_yugen_ep_count(client, &yugen_id, &yugen_slug).await;

    loop {
        let status = if anilist_result.episode == progress + 1 {
            "COMPLETED"
        } else {
            "CURRENT"
        };
        let (video_url, ep_title) =
            get_video_url_and_title(client, &yugen_id, &yugen_slug, progress).await?;

        let mpv_output = play_video(&video_url, &ep_title)?;
        let watch_percentage =
            parse_mpv_output(&mpv_output).ok_or(Error::IO(IOError::ParseMpvOutput))?;

        if watch_percentage > 85 {
            update_progress(
                client,
                token,
                status,
                progress + 1,
                &anilist_result.id.to_string(),
            )
            .await?;
        }

        if episodes == progress + 1 {
            let score = get_input("Enter new score: ").unwrap().parse().unwrap();
            score_on_completion(client, score, token, anilist_result.id)
                .await
                .unwrap();
            break;
        }

        if get_input("Continue: ")? != "y" {
            break;
        }

        progress += 1;
    }
    Ok(())
}

pub fn parse_mpv_output(output: &str) -> Option<u16> {
    Regex::new(r"(?m)\(([0-9]+)%\)")
        .unwrap()
        .captures_iter(output)
        .next()?
        .get(1)?
        .as_str()
        .to_string()
        .parse()
        .ok()
}

pub fn args() -> Option<(String, String)> {
    use std::env;
    let args: Vec<String> = env::args().skip(1).collect();
    if args.len() == 2 {
        return Some((args[0].clone(), args[1].clone()));
    }
    None
}

pub fn read_auth_files(token_path: &str, user_id_path: &str) -> Result<(String, String), Error> {
    use std::fs;
    Ok((
        fs::read_to_string(token_path).map_err(|e| Error::IO(IOError::ReadTokenFile(e)))?,
        fs::read_to_string(user_id_path).map_err(|e| Error::IO(IOError::ReadUserIdFile(e)))?,
    ))
}

pub async fn no_anilist(client: &Client, query: &str) -> Result<(), Error> {
    let search_result = search(client, query).await?;

    if search_result.is_empty() {
        return Err(Error::Yugen(YugenError::EmptySearchResult));
    }

    search_result
        .iter()
        .enumerate()
        .for_each(|(i, r)| println!("{} [{}]", r.title, i + 1));

    let anime_choice: usize = get_input("Choose anime: ")?
        .parse()
        .map_err(|e| Error::IO(IOError::ParseInput(e)))?;

    let anime = search_result
        .get(anime_choice - 1)
        .ok_or(Error::IO(IOError::AnimeChoiceOutOfBounds))?;

    let ep_choice: u32 = get_input("Choose episode: ")?
        .parse()
        .map_err(|e| Error::IO(IOError::ParseInput(e)))?;

    let ep_page = download_ep_page(client, anime.url.to_owned(), ep_choice).await?;
    let yugen_id = get_id(&ep_page)?;
    let link_response = get_ep_link_response(client, &yugen_id).await?;
    let video_url = parse_link_response(&link_response)?;
    let ep_title = get_ep_title(&ep_page, ep_choice)?;
    let ep_title = decode_html_entities(&ep_title);
    let output = play_video(&video_url, &ep_title)?;
    parse_mpv_output(&output).ok_or(Error::IO(IOError::ParseMpvOutput))?;
    Ok(())
}

pub async fn get_ids(
    client: &Client,
    data: &str,
) -> Result<(String, String, WatchingAnime), Error> {
    let watching_result = get_watching_anime(data);
    if watching_result.is_empty() {
        return Err(Error::Anilist(AnilistError::EmptyWatchingList));
    }

    watching_result
        .iter()
        .enumerate()
        .for_each(|(i, m)| println!("{} [{}]", m.title, i + 1));

    let input: usize = get_input("Enter choice: ")?
        .parse()
        .map_err(|e| Error::IO(IOError::ParseInput(e)))?;

    let anilist_id = &watching_result[input - 1].media_id;
    println!("anilist id: {anilist_id}");

    let bal_mackup_data = bal_mackup_data(client, &anilist_id.to_string()).await?;
    let (id, slug) = anilist_to_yugen(&bal_mackup_data).await?;

    Ok((id, slug, watching_result[input - 1].clone()))
}

pub fn play_video(url: &str, title: &str) -> Result<String, Error> {
    let child = std::process::Command::new("mpv")
        .arg(&format!("--force-media-title={title}"))
        .arg(url)
        .stdout(std::process::Stdio::piped())
        .spawn()
        .map_err(|e| Error::IO(IOError::MpvSpawn(e)))?;

    String::from_utf8(
        child
            .wait_with_output()
            .map_err(|e| Error::IO(IOError::MpvOutput(e)))?
            .stdout,
    )
    .map_err(|e| Error::IO(IOError::MpvInvalidUtf8(e)))
}

pub async fn get_token_and_id(client: &Client) -> Result<(String, String), Error> {
    use std::fs;
    if let Some((token_path, user_id_path)) = args() {
        return read_auth_files(&token_path, &user_id_path);
    }

    let data_dir = dirs::data_dir().ok_or(Error::IO(IOError::DataDirNotFound))?;
    let data_dir = data_dir
        .to_str()
        .to_owned()
        .ok_or(Error::IO(IOError::DataDirNotFound))?;

    let (token_path, user_id_path) = (
        format!("{data_dir}/yugen-rs/anilist_token.txt"),
        format!("{data_dir}/yugen-rs/anilist_user_id.txt"),
    );

    if fs::read_dir(data_dir.to_owned() + "/yugen-rs").is_err() {
        fs::create_dir_all(data_dir.to_owned() + "/yugen-rs")
            .map_err(|e| Error::IO(IOError::CreateDataDir(e)))?;
    }

    if fs::read(&token_path).is_err() {
        let input = get_input(
            "Paste your access token from this page:
https://anilist.co/api/v2/oauth/authorize?client_id=9857&response_type=token : ",
        )?
        .trim()
        .to_owned();
        fs::write(&token_path, input).map_err(|e| Error::IO(IOError::CreateToken(e)))?;
    }
    if fs::read(&user_id_path).is_err() {
        let data = get_user_id_data(
            client,
            &fs::read_to_string(&token_path).map_err(|e| Error::IO(IOError::ReadTokenFile(e)))?,
        )
        .await?;
        let user_id = parse_user_id(&data)?;
        fs::write(&user_id_path, user_id).map_err(|e| Error::IO(IOError::CreateUserId(e)))?;
    }
    read_auth_files(&token_path, &user_id_path)
}
