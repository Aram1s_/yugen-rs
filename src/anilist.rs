use crate::Error;
use regex::Regex;
use reqwest::Client;

const ANILIST_BASE: &str = "https://graphql.anilist.co";
const BAL_MACKUP_BASE: &str =
    "https://raw.githubusercontent.com/bal-mackup/mal-backup/master/anilist/anime/";

#[derive(Debug)]
#[allow(unused)]
pub enum AnilistError {
    AnilistFailedToFetchData(reqwest::Error),
    NoAnimeFound,
    EmptyWatchingList,
    FailedToConvertId,
    BalMackupDataNotFound(reqwest::Error),
    FailedToFetchProgress,
    FailedToUpdateProgress(reqwest::Error),
    FetchUserIdData(reqwest::Error),
    GetUserIdData,
    FetchSearchResult(reqwest::Error),
    GetSearchResult,
    ScoreOnCompletion(reqwest::Error),
}

use AnilistError::*;

pub async fn get_anilist_data(client: &Client, user_id: &str) -> Result<String, Error> {
    client
        .post(ANILIST_BASE)
        .header("Content-Type", "application/json")
        .body("{\"query\":\"query($userId:Int,$userName:String,$type:MediaType){MediaListCollection(userId:$userId,userName:$userName,type:$type){lists{name isCustomList isCompletedList:isSplitCompletedList entries{...mediaListEntry}}user{id name avatar{large}mediaListOptions{scoreFormat rowOrder animeList{sectionOrder customLists splitCompletedSectionByFormat theme}mangaList{sectionOrder customLists splitCompletedSectionByFormat theme}}}}}fragment mediaListEntry on MediaList{id mediaId status score progress progressVolumes repeat priority private hiddenFromStatusLists customLists advancedScores notes updatedAt startedAt{year month day}completedAt{year month day}media{id title{userPreferred romaji english native}coverImage{extraLarge large}type format status(version:2)episodes volumes chapters averageScore popularity isAdult countryOfOrigin genres bannerImage nextAiringEpisode{airingAt timeUntilAiring episode} startDate{year month day}}}\",\"variables\":{\"userId\":".to_string() + 
              user_id + ",\"type\":\"ANIME\"}}")
        .send()
        .await.map_err(|e| Error::Anilist(AnilistFailedToFetchData(e)))?.text().await.map_err(|e| Error::Anilist(AnilistFailedToFetchData(e)))
}

#[derive(Clone)]
pub struct WatchingAnime {
    pub media_id: u32,
    pub status: String,
    pub title: String,
    pub episodes: u32,
}

pub fn get_watching_anime(data: &str) -> Vec<WatchingAnime> {
    let regex = Regex::new(r#"(?mU)mediaId":([0-9]+),"status":"([A-Z]+)".*english":"([^"]*)".*episodes":([0-9]+),"#).unwrap();
    regex
        .captures_iter(data)
        .map(|m| WatchingAnime {
            media_id: m.get(1).unwrap().as_str().parse().unwrap(),
            status: m.get(2).unwrap().as_str().parse().unwrap(),
            title: m.get(3).unwrap().as_str().parse().unwrap(),
            episodes: m.get(4).unwrap().as_str().parse().unwrap(),
        })
        .filter(|m| m.status != "COMPLETED" && m.status != "PAUSED" && m.status != "DROPPED")
        .collect()
}

pub fn get_progress(data: &str, id: &str) -> Result<String, Error> {
    Ok(Regex::new(&format!(
        r#"mediaId":{id},"status":"CURRENT","score":[^,]*,"progress":([0-9]+)"#
    ))
    .unwrap()
    .captures_iter(data)
    .next()
    .ok_or(Error::Anilist(FailedToFetchProgress))?
    .get(1)
    .ok_or(Error::Anilist(FailedToFetchProgress))?
    .as_str()
    .to_owned())
}

pub async fn bal_mackup_data(client: &Client, id: &str) -> Result<String, Error> {
    client
        .get(BAL_MACKUP_BASE.to_owned() + id + ".json")
        .send()
        .await
        .map_err(|e| Error::Anilist(BalMackupDataNotFound(e)))?
        .text()
        .await
        .map_err(|e| Error::Anilist(BalMackupDataNotFound(e)))
}

pub async fn anilist_to_yugen(data: &str) -> Result<(String, String), Error> {
    let r = Regex::new(r#"(?m)url":\s"https://yugenanime.tv/anime/([0-9]+)/([^/]+)"#).unwrap();
    let caps = r
        .captures_iter(data)
        .next()
        .ok_or(Error::Anilist(FailedToConvertId))?;
    let mut caps = caps.iter().skip(1);
    Ok((
        caps.next()
            .ok_or(Error::Anilist(FailedToConvertId))?
            .ok_or(Error::Anilist(FailedToConvertId))?
            .as_str()
            .to_owned(),
        caps.next()
            .ok_or(Error::Anilist(FailedToConvertId))?
            .ok_or(Error::Anilist(FailedToConvertId))?
            .as_str()
            .to_owned(),
    ))
}

pub async fn update_progress(
    client: &Client,
    token: &str,
    status: &str,
    progress: u32,
    media_id: &str,
) -> Result<(), Error> {
    let progress = &progress.to_string();
    client
        .post(ANILIST_BASE)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer ".to_string() + token)
        .body("{\"query\":\"mutation($id:Int $mediaId:Int $status:MediaListStatus $score:Float $progress:Int $progressVolumes:Int $repeat:Int $private:Boolean $notes:String $customLists:[String]$hiddenFromStatusLists:Boolean $advancedScores:[Float]$startedAt:FuzzyDateInput $completedAt:FuzzyDateInput){SaveMediaListEntry(id:$id mediaId:$mediaId status:$status score:$score progress:$progress progressVolumes:$progressVolumes repeat:$repeat private:$private notes:$notes customLists:$customLists hiddenFromStatusLists:$hiddenFromStatusLists advancedScores:$advancedScores startedAt:$startedAt completedAt:$completedAt){id mediaId status score advancedScores progress progressVolumes repeat priority private hiddenFromStatusLists customLists notes updatedAt startedAt{year month day}completedAt{year month day}user{id name}media{id title{userPreferred}coverImage{large}type format status episodes volumes chapters averageScore popularity isAdult startDate{year}}}}\",\"variables\":{\"status\":\"".to_owned() + status + "\",\"progress\":" + progress + ",\"mediaId\":" + media_id + "}}")
        .send().await.map_err(|e| Error::Anilist(FailedToUpdateProgress(e)))?;
    Ok(())
}

pub async fn get_user_id_data(client: &Client, token: &str) -> Result<String, Error> {
    client
        .post(ANILIST_BASE)
        .header("Content-Type", "application/json")
        .header("Accept", "application/json")
        .header("Authorization", "Bearer ".to_owned() + token)
        .body("{\"query\":\"query { Viewer { id } }\"}")
        .send()
        .await
        .map_err(|e| Error::Anilist(FetchUserIdData(e)))?
        .text()
        .await
        .map_err(|e| Error::Anilist(FetchUserIdData(e)))
}

pub fn parse_user_id(data: &str) -> Result<String, Error> {
    Ok(Regex::new(r#"(?m)"id":([0-9]*)"#)
        .unwrap()
        .captures_iter(data)
        .next()
        .ok_or(Error::Anilist(GetUserIdData))?
        .get(1)
        .ok_or(Error::Anilist(GetUserIdData))?
        .as_str()
        .to_owned())
}

pub async fn anilist_search_data(client: &Client, query: &str) -> Result<String, Error> {
    client.post(ANILIST_BASE)
        .header("Content-Type", "application/json")
        .body("{\"query\":\"query($page:Int = 1 $id:Int $type:MediaType $isAdult:Boolean = false $search:String $format:[MediaFormat]$status:MediaStatus $countryOfOrigin:CountryCode $source:MediaSource $season:MediaSeason $seasonYear:Int $year:String $onList:Boolean $yearLesser:FuzzyDateInt $yearGreater:FuzzyDateInt $episodeLesser:Int $episodeGreater:Int $durationLesser:Int $durationGreater:Int $chapterLesser:Int $chapterGreater:Int $volumeLesser:Int $volumeGreater:Int $licensedBy:[Int]$isLicensed:Boolean $genres:[String]$excludedGenres:[String]$tags:[String]$excludedTags:[String]$minimumTagRank:Int $sort:[MediaSort]=[POPULARITY_DESC,SCORE_DESC]){Page(page:$page,perPage:20){pageInfo{total perPage currentPage lastPage hasNextPage}media(id:$id type:$type season:$season format_in:$format status:$status countryOfOrigin:$countryOfOrigin source:$source search:$search onList:$onList seasonYear:$seasonYear startDate_like:$year startDate_lesser:$yearLesser startDate_greater:$yearGreater episodes_lesser:$episodeLesser episodes_greater:$episodeGreater duration_lesser:$durationLesser duration_greater:$durationGreater chapters_lesser:$chapterLesser chapters_greater:$chapterGreater volumes_lesser:$volumeLesser volumes_greater:$volumeGreater licensedById_in:$licensedBy isLicensed:$isLicensed genre_in:$genres genre_not_in:$excludedGenres tag_in:$tags tag_not_in:$excludedTags minimumTagRank:$minimumTagRank sort:$sort isAdult:$isAdult){id title{userPreferred}coverImage{extraLarge large color}startDate{year month day}endDate{year month day}bannerImage season seasonYear description type format status(version:2)episodes duration chapters volumes genres isAdult averageScore popularity nextAiringEpisode{airingAt timeUntilAiring episode}mediaListEntry{id status}studios(isMain:true){edges{isMain node{id name}}}}}}\",\"variables\":{\"page\":1,\"type\":\"ANIME\",\"sort\":\"SEARCH_MATCH\",\"search\":\"".to_owned() + query + "\"}}")
        .send()
        .await
        .map_err(|e| Error::Anilist(FetchSearchResult(e)))?
        .text()
        .await.map_err(|e| Error::Anilist(FetchSearchResult(e)))
}

#[derive(Debug)]
pub struct AnilistResult {
    pub id: u32,
    pub name: String,
    pub episode: u32,
}

pub fn get_anilist_search_result(data: &str) -> Vec<AnilistResult> {
    let regex =
        Regex::new(r#"(?mU)"id":([0-9]+),.*"userPreferred":"([^"]+)"}.*"episode"s*["]*:([0-9]+)"#)
            .unwrap();
    let data = data.replace("\\/", "/");
    regex
        .captures_iter(&data)
        .map(|m| AnilistResult {
            id: m.get(1).unwrap().as_str().parse().unwrap(),
            name: m.get(2).unwrap().as_str().parse().unwrap(),
            episode: m.get(3).unwrap().as_str().parse().unwrap(),
        })
        .collect()
}

pub async fn score_on_completion(
    client: &Client,
    score: f32,
    token: &str,
    media_id: u32,
) -> Result<(), Error> {
    client.post(ANILIST_BASE)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer ".to_owned() + token)
        .body("{\"query\":\"mutation($id:Int $mediaId:Int $status:MediaListStatus $score:Float $progress:Int $progressVolumes:Int $repeat:Int $private:Boolean $notes:String $customLists:[String]$hiddenFromStatusLists:Boolean $advancedScores:[Float]$startedAt:FuzzyDateInput $completedAt:FuzzyDateInput){SaveMediaListEntry(id:$id mediaId:$mediaId status:$status score:$score progress:$progress progressVolumes:$progressVolumes repeat:$repeat private:$private notes:$notes customLists:$customLists hiddenFromStatusLists:$hiddenFromStatusLists advancedScores:$advancedScores startedAt:$startedAt completedAt:$completedAt){id mediaId status score advancedScores progress progressVolumes repeat priority private hiddenFromStatusLists customLists notes updatedAt startedAt{year month day}completedAt{year month day}user{id name}media{id title{userPreferred}coverImage{large}type format status episodes volumes chapters averageScore popularity isAdult startDate{year}}}}\",\"variables\":{\"score\":".to_owned() + &score.to_string() + ",\"mediaId\":" + &media_id.to_string() + "}}")
        .send().await.map_err(|e| Error::Anilist(ScoreOnCompletion(e)))?;
    Ok(())
}
