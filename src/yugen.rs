use crate::Error;
use regex::Regex;
use reqwest::Client;
use YugenError::*;

const BASE: &str = "https://yugenanime.tv/";
const API: &str = "https://yugenanime.tv/api/embed/";

#[derive(Debug)]
pub enum YugenError {
    DownloadSearchResult(reqwest::Error),
    DownloadFromId(reqwest::Error),
    DownloadEpPage(reqwest::Error),
    IdNotFound,
    EpLinkResponse(reqwest::Error),
    VideoLinkNotFound,
    EpTitleNotFound,
    EmptySearchResult,
}

// #[derive(Debug)]
// enum Genre {
//     Action,
//     Adventure,
//     SuperPower,
//     Fantasy,
//     Shounen,
// }

#[derive(Debug)]
pub struct Anime {
    pub title: String,
    pub url: String,
    // pub img_url: String,
    // pub episode_count: u32,
    // pub anilist_id: u32,
    // pub yugen_id: u32,
    // pub yugen_slug: String,
    // episode_duration: Duration,
    // genre_list: Vec<Genre>,
}

pub fn get_ep_title(html: &str, ep_number: u32) -> Result<String, Error> {
    Ok(Regex::new(&format!(r#"(?m)>{ep_number}\s:\s([^<]*)"#))
        .unwrap()
        .captures_iter(html)
        .next()
        .ok_or(Error::Yugen(EpTitleNotFound))?
        .get(1)
        .ok_or(Error::Yugen(EpTitleNotFound))?
        .as_str()
        .to_string())
}

pub async fn download_ep_page(
    client: &Client,
    url: String,
    ep_number: u32,
) -> Result<String, Error> {
    client
        .get(url + &ep_number.to_string() + "/")
        .send()
        .await
        .map_err(|e| Error::Yugen(DownloadEpPage(e)))?
        .text()
        .await
        .map_err(|e| Error::Yugen(DownloadEpPage(e)))
}

pub fn parse_link_response(response: &str) -> Result<String, Error> {
    Ok(Regex::new(r#"(?m)"hls":\s\["([^"]*)"#)
        .unwrap()
        .captures_iter(response)
        .next()
        .ok_or(Error::Yugen(VideoLinkNotFound))?
        .get(1)
        .ok_or(Error::Yugen(VideoLinkNotFound))?
        .as_str()
        .to_string())
}

pub async fn get_ep_link_response(client: &Client, id: &str) -> Result<String, Error> {
    client
        .post(API)
        .header("X-Requested-With", "XMLHttpRequest")
        .form(&[("id", id), ("ac", "0")])
        .send()
        .await
        .map_err(|e| Error::Yugen(EpLinkResponse(e)))?
        .text()
        .await
        .map_err(|e| Error::Yugen(EpLinkResponse(e)))
}

pub fn get_id(html: &str) -> Result<String, Error> {
    Ok(
        Regex::new(r#"(?m)id="main-embed" src="//yugenanime\.tv/e/([^"]*)/""#)
            .unwrap()
            .captures_iter(html)
            .next()
            .ok_or(Error::Yugen(IdNotFound))?
            .get(1)
            .ok_or(Error::Yugen(IdNotFound))?
            .as_str()
            .to_string(),
    )
}

pub async fn search(client: &Client, query: &str) -> Result<Vec<Anime>, Error> {
    let result = client
        .get(BASE.to_owned() + "discover/?q=" + query)
        .send()
        .await
        .map_err(|e| Error::Yugen(DownloadSearchResult(e)))?
        .text()
        .await
        .map_err(|e| Error::Yugen(DownloadSearchResult(e)))?;
    Ok(regex(&result))
}

fn regex(html: &str) -> Vec<Anime> {
    Regex::new(r#"(?m)anime-meta"\shref="/anime/([0-9]+/[^"/]+)[^"]*"\stitle="([^"]*)"><div class="anime-poster__container"><img data-src="([^"]*)"#).unwrap()
        .captures_iter(html)
        .filter_map(|mat| {
            Some(Anime {
                title: html_escape::decode_html_entities(mat.get(2)?.as_str()).to_string(),
                url: BASE.to_owned() + "watch/" + mat.get(1)?.as_str() + "/",
                // img_url: mat.get(3)?.as_str().to_string(),
            })
        })
        .collect()
}

pub async fn download_from_id(
    client: &Client,
    yugen_id: &str,
    yugen_slug: &str,
    progress: u32,
) -> Result<String, Error> {
    // https://yugenanime.tv/watch/ID/SLUG/EP_NUMBER/
    let url = format!("{BASE}watch/{yugen_id}/{yugen_slug}/{}/", progress + 1);
    client
        .get(url)
        .send()
        .await
        .map_err(|e| Error::Yugen(DownloadFromId(e)))?
        .text()
        .await
        .map_err(|e| Error::Yugen(DownloadFromId(e)))
}

pub async fn get_yugen_ep_count(client: &Client, yugen_id: &str, yugen_slug: &str) -> u32 {
    let page = client
        .get(BASE.to_owned() + "anime/" + yugen_id + "/" + yugen_slug + "/watch/")
        .send()
        .await
        .unwrap()
        .text()
        .await
        .unwrap();
    Regex::new(
        r#"(?m)Episodes</div><span class="description" style="font-size: 12px;">([0-9]+)<"#,
    )
    .unwrap()
    .captures_iter(&page)
    .next()
    .unwrap()
    .get(1)
    .unwrap()
    .as_str()
    .to_owned().parse().unwrap()
}
