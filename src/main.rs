use anilist::*;
use custom_error::*;
use reqwest::Client;
use tokio::main;
use util::*;
use yugen::*;

mod anilist;
mod custom_error;
mod util;
mod yugen;

#[main]
async fn main() {
    let client = Client::new();

    let arg = std::env::args().nth(1);
    if arg == Some("--no-anilist".to_string()) {
        let query = match get_input("Enter search: ") {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Error: {e}");
                std::process::exit(1);
            }
        };
        if let Err(e) = no_anilist(&client, &query).await {
            eprintln!("Error: {e}");
        }
        return;
    }

    let (token, user_id) = match get_token_and_id(&client).await {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error: {e}");
            std::process::exit(1);
        }
    };

    if arg == Some("--new".to_owned()) {
        let query = match get_input("Enter search: ") {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Error: {e}");
                std::process::exit(1);
            }
        };
        let data = anilist_search_data(&client, &query).await.unwrap();
        let result = get_anilist_search_result(&data);
        result
            .iter()
            .enumerate()
            .for_each(|(i, m)| println!("{} [{}]", m.name, i + 1));

        let choice: usize = match get_input("Enter choice: ") {
            Ok(v) => v.parse().unwrap(),
            Err(e) => {
                eprintln!("Error: {e}");
                std::process::exit(1);
            }
        };
        watch_new(&client, &token, &result[choice - 1])
            .await
            .unwrap();
        return;
    }

    match run(&client, &token, &user_id).await {
        Ok(_) => (),
        Err(e) => {
            eprintln!("Error: {e}");
            std::process::exit(1);
        }
    }
}
